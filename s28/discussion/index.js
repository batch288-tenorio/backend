// show databases - list of the db inside our cluster
// use 'dbName' - to use a specific database
// show colletions - to see the list of collections inside the db
// help - to show commands
// cls - to clear

// CRUD operations
/*
	- CRUD operation is the heart of any backend application.
	- mastering the CRUD operation is essential for any developer especially to those who want to become backened developer.
*/

//[Section] Inserting Document (Create)
	// Insert one document 
		/*
			- Since mongoDB deals with objects as it's structure for documents we can easily create them by providing objects in our method / operation.

			Syntax:
				db.collectionName.insertOne({
					object
				})
		*/

db.users.insertOne({
	firstName : "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "python"],
	department: "none"
})

// Insert Many

/*
	syntax:
		db.collectionName.insertMany([{objectA}, {objectB}])
*/

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},

	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "87654321",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	}
])

db.userss.insertOne({
	firstName : "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "python"],
	department: "none"
})

// [SECTION] Finding documents (Read Operation)
	// db.collectionName.find();
	// db.collectionName.find({field:value});


// using the find() method, it will show you the list of all the documents inside our collection 
db.users.find();


// the "pretty" methods allows us to be able to view the docments returned  by or terminals to be in a better format.
db.users.find().pretty();

// it will return the documents that will pass the criteria given in the method
db.users.find({firstName: "Stephen"});

db.users.find({"_id" : ObjectId("646c557ce294ee160500ff15")});

// multiple criteria
db.users.find({lastName : "Armstrong", age: 82});

db.users.find({firstName: "Jane"});
db.users.find({firstName: "Jane", age: 21});
db.users.find("contact.phone" : "87654321");

// [Section] Updating documents(Update)

db.users.insertOne({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact: {
		phone: "0000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

// updateOne method

/*
	syntax:
		db.collectionName.updateOne({criterial, {$set: {field:value}}});
*/

db.users.updateOne(
	{ firstName: "test"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "none"
		}
	})

db.users.updateOne(
	{ firstName: "Chris"},
	{
		$set: {
			firstName: "Omen",	
			age: 24,
		}
	})

db.users.updateOne(
	{ firstName: "Omen"},
	{
		$set: {
			firstName: "Bill",	
			age: 65,
		}
	})

db.users.updateOne(
	{ firstName: "Bill"},
	{
		$set: {
			firstName: "Chris",			
		}
	})

db.users.updateOne(
	{ firstName: "Jane"},
	{
		$set: {
			lastName: "Edited"			
		}
	});

// updating multiple documents
/*
	syntax:
		db.collectionName.updateMany(
		{criteria},
		{
			$set: {
				{field:value}
			}
		}
		)
*/

db.users.updateMany(
{department: "none"},
{
	$set: {
		department: "HR"
	}
}
)

// Replace One
	/*
		Syntax: db.collectionName.replaceOne(
		{criteria}, {
			$set: {
			object
			}
		}
		)
	*/

db.users.insertOne({firstName: "test"});

db.users.replaceOne(
	{firstName: "test"}
		{
			firstName: "Bill",
			lastName: "Gates",
			age: 65.
			contact: {},
			courses: [],
			department: "Operations"
		}
	)

db.users.replaceOne(
	{ firstName: "Stephen"},
	{
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {},
		courses: [],
		department: "Operations"
	}
	)


// Section- Deleting documents
// deleting single documents

	/*
		db.collectionName.deleteOne({criteria})
	*/

db.users.deleteOne({firstName: "Chris"});

// deleting multiple documents

/*
	db.collectionName.deleteMany({criteria})
*/

db.users.deleteMany({firstName: "Jane"});

// all the doduments in our collection will be deleted
db.users.deleteMany({});
