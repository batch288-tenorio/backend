const express = require('express');
const mongoose = require('mongoose');

const tasksRoutes = require('./routers/tasksRoutes.js');

const port = 4000;

const app = express();

// set up MongoDD connection
mongoose.connect("mongodb+srv://admin:admin@batch288tenorio.oxntzoo.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser: true});

// check connection
const db = mongoose.connection;

	db.on("error", console.error.bind(console, "Error, can't connect to the db!"));

	db.once("open", () => console.log("We are now connected to the db!"));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// this route/middleware is responsible 
app.use("/tasks", tasksRoutes);


if(require.main === module) {
	app.listen(port, () => console.log(`The server is running at port ${port}!`));
}
module.exports = app;