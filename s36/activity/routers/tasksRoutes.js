const express = require("express");

const taskControllers = require('../controllers/taskControllers.js');

// contains all the endpoints of our application
const router = express.Router();

router.get("/", taskControllers.getAllTasks);

router.post("/addTask", taskControllers.addTasks);

router.get("/:id", taskControllers.getSpecificTask);

router.put("/:id/complete", taskControllers.changeTaskStatus);
// parameterizer

// we are going to create a route using a delete method at the URL "/tasks/:id"
// the colon here is an identifier that helps create a dynamic route which allows us to supply information
// pag may colon parameter yan
router.delete("/:id", taskControllers.deleteTask);

module.exports = router;