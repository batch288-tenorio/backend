// console.log('Hello Batch 288');

// [SECTION] Javascript Synchronous vs Asynchronous
	// Javascript is by default synchronous meaning that only one statement is executed a time.

	// this can be proven when a statement has an error, javascript will not proceed with the next statement

	/*console.log('hello world');

	conole.log('hello after the world');

	console.log('hello');*/

	// when certain statements take a lot of time to process, this slows down our code.

	/*for(let index = 0; index <= 1500; index++){
		console.log(index);
	}
	console.log('hello again');*/

	// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background.

// [SECTION] getting all posts 
	// the Fetch API allows you to asynochronously request for a resource data.
	// so it means the fetch method that we are going to use here will run asynchronously.

	// syntax:
		// fetch('URL');

	// a "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
	console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

	// syntax:
		/*
			fetch('URL')
			.then((response => response));
		*/

	// retrieve all posts follow the REST API
	// the fetch method will return a promise that resolves the response object.
	// the "then" method captures the response object 
	// use the json method from the response object to convert the data retrieved into JSON format to be used in our application
	fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response => response.json())
	.then(json => console.log(json))

	// the "async" and "await" keyword, it is another approach that can be used to achieve asynchronous code - used in function only

	// creates an asynchronous function
	async function fetchData(){

		let result = await fetch('https://jsonplaceholder.typicode.com/posts');
		console.log(result);

		let json = await result.json();
		console.log(json);

	};
	fetchData();

	//[SECTION] Getting specific post

	//Retrives specific post following the rest API(/posts/:id) 

	fetch('https://jsonplaceholder.typicode.com/posts/5')
	.then(response => response.json())
	.then(json => console.log(json));

	// [SECTION] Creating posts
	// syntax:
		
			// options is an object that contains the method, the headers and the body of the request

			// by default, if ypu dont add the method in the fetch request, it will be a GET Method

			/*fetch('URL', options)
			.then(response => {})
			.then(response => {})*/
		
		fetch('https://jsonplaceholder.typicode.com/posts', {
			// sets the method of the request object to post following the rest API
			method: 'POST', 
			// sets the header data of the request object to be sent to the backend
			// specified that the content will be in JSON structure
			headers: {
				'Content-type': 'application/json'
			},
			// sets the contect/body data of the request object to be sent backend
			body: JSON.stringify({
					title : 'New post',
					body: 'Hello World',
					userId: 1
			})
		})
		.then(response => response.json())
		.then(json => console.log(json));

		// [SECTION] Update a specific post

		// PUT method
		fetch('https://jsonplaceholder.typicode.com/posts/1', {
			method: 'PUT',
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
					title: 'Updated Post'		
			})
		})
		.then(response => response.json())
		.then(json => console.log(json));

		// PATCH
		fetch('https://jsonplaceholder.typicode.com/posts/1', {
			method: 'PATCH',
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
					title: 'Updated Post'					
			})
		})
		.then(response => response.json())
		.then(json => console.log(json));

		// The PUT Method is a method of modifying resource where the client sends data that updates the entire object/document

		// patch method applies a partial update to the object or document.

		// [SECTION] Deleting a post 

		// deletin a specific post following the REST API
		fetch('https://jsonplaceholder.typicode.com/posts/1', {method: 'DELETE'})
		.then(response => response.json())
		.then(json => console.log(json));