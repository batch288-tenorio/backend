// console.log('hello world!');

// Conditional statements allows us to control the flow of our program. It allows us to run a statement or instruction if a condition is met or run separate instruction if otherwise.

//  [Section] if, else if and else statement

let numA = -1;

// if statement
	// execute a statement if the specified condition is met or true

if (numA < 0){
	console.log('Hello');
}

/*
	Syntax:
		if(condition){
			statement;
		}
*/

// The result of the expression in the if's condition must result to true, else, the statement inside the curly {} will not run.

console.log(numA<0);

// Let's update the variable and run if statement with the same condition

numA = 0;

if(numA <0){
	console.log('Hello again in numA is 0!');
}

// It will not run because the expression now results to false:
console.log(numA < 0);

// Let's take a look at another example:

let city = "New York";

// strict equality operator (data type sensitive)
if (city === "New York"){
	console.log('Welcome to New York City');
}
console.log(city === "New York");

// else if statement
	/*
		-execute a statement if previous conditions are false and if specified condition is true.
		-the 'else if' statement is optional and can be addes to capture additional conditions to change the flow of a program.
	*/

let numH = 1;

if (numH > 2){
	console.log('Hello');	
} 
else if (numH < 2){
	console.log('World');
}

// we we're able to run the else if () statement after we evaulated that the if condition was false.

numH = 2

if (numH === 2 ){
	console.log('Hello');
}
else if (numH > 1){
	console.log('World');
}

//  else if () statement was no longer run because the if statement was able to run, the evaluation of the whole statement stops there.

city = "Tokyo";

if (city === "New York"){
	console.log('Welcome to New York City!');
}
else if (city === 'Manila'){
	console.log("Welcome to Manila City, Ph!");
}
else if (city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan");
}

// since we failed the condition for the if () and the first else if(), we went to the second else if () and checked.

// else statement
/*
	-executes a statement if all other conditions are false.
	-the "else" statement is only optional and can be added to capture any other possible result to change the flow of a program.
*/

let numB = 0;

if (numB > 0 ){
	console.log('hello from numB!');
}
else if (numB < 0 ){
	console.log('World from B!');
}
else{
	console.log('Again from numB');
}

	/*
		Since the preceding if and else if conditions failed, the else statement was run instead.
	*/

// it will cause an error
/*else {
	console.log('will not run without an if!');
}*/

/*else if (numB === 2){
	console.log('this will cause an error!');
}*/

// if, else if and else statement with functions

	/*
		Most of the times we would like to use if, else if, and else statements with functionsto control the flow of our program.
	*/

	// we are going to create a function that will tell the typhoon intensity by providing the winds speed.

	function determineTyphoonIntensity(windSpeed){
		if(windSpeed < 0){
			return "Invalid wind speed";
		}
		else if (windSpeed <= 38 && windSpeed >= 0){
			return "Tropical Depression detected!";
		}
		else if (windSpeed >= 39 && windSpeed <= 73){
			return "Tropical Storm detected!";
		}
		else if (windSpeed >=74 && windSpeed <= 95){
			return "Signal No. 1";
		}
		else if (windSpeed >=96 && windSpeed <= 110){
			return "Signal No. 2";
		}
		else if (windSpeed >=111 && windSpeed <= 129){
			return "Signal No. 3";
		}
		else if (windSpeed >=130 && windSpeed <= 156){
			return "Signal No. 4";
		}
		else {
			return "Signal No. 5";
		}
	}

	console.log(determineTyphoonIntensity(30));
	console.log(determineTyphoonIntensity(-1));
	console.log(determineTyphoonIntensity(157));

	// console.warn() is a good way to print warinings in our console that could help us developers act on a certain output within our.
	console.warn(determineTyphoonIntensity(40));

// [Section] Truthy and Falsy
	// In Javascript a truthy value is a value that is considered true when encountered in a boolean context.
	// Falsy value / exemption for truthy:
	/*
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN
	*/

	// Truthy

	if(true){
		console.log('Truthy');
	}

	if(1){
		console.log('Truthy');
	}

	// Falsy Examples

	if(false){
		console.log('Falsy');
	}

	if(0){
		console.log('Falsy');
	}

// [Section] Conditional (Ternary) Operator
/*
	-The ternary operator takes in three operands:
		1. condition
		2. expression to execute if the condition is truthy
		3. expression to execute if the condition is falsy
	- it can be used to an if else statement
	- ternary operators have an implicit return statement meaning without 'return' the resulting expression can be stored in a variable.

		-syntax:
			condition ? ifTrue : ifFalse;
*/

	// single statement execution

	let ternaryResult = (1 < 18) ? true : false;

	console.log('result of ternary operator: '+ ternaryResult);

	let exampleTernary = (0) ? "The number is not equal to zero" : "The number is equal to zero";

	console.log(exampleTernary);

	// multiple statement execution

	function isOfLegalAge(){
		let name = 'John'
		return "You are in the legal age limit, " + name;
	}

	function isUnderAge(){
		let name = 'John'
		return 'you are under the age limit, ' + name;
	}
	

	/*let age = parseInt(prompt('What is your age?'));
	console.log(age);
	console.log(typeof age);

	let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();

	console.log(legalAge);*/

	// the parseInt function converts the input receive as a string data type into number.

// [Section] Switch statement
	// switch statement evaluates an expression and matches the expression's value to a case clause. the switch will then execute the statements associated with that case, as well as statements in cases that follow the matching case.


	// the break statement is used to terminate the current loop once match has been found

	/*
		Syntax:
		swich (expression/variable){
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}
	*/

	// the ".toLowerCase() function/method will change the inout received from the prompt into all lowercase letters"

	let day = prompt('what is of the week is today?').toLowerCase();

	console.log(day);
	switch(day){
		case 'monday':
			console.log('the color of the day is red!');
			break;
		case 'tuesday':
			console.log('the color of the day is orange');
			break;
		case 'wednesday':
			console.log('the color of the day is yellow');
			break;
		case 'thursday':
			console.log('the color of the day is green');
			break;
		case 'friday':
			console.log('the color of the day is blue');
			break;
		case 'saturday':
			console.log('the color of the day is indigo');
			break;
		case 'sunday':
			console.log('the color of the day is violet');
			break;

		default:
			console.log('please input a valid day');
			break;
	}

// [Section] Try - catch - finally statement
	// try catch statements are commonly used for error handling.
	//  there are instances when the application returns an error/  warning that is not necessarily an error in context of our code.

	function showIntensityAlert(windSpeed){
		try{
			alert(determineTyphoonIntensity(windSpeed))
		}
		// the catch will only run if and only if there was an error in the statement inside our try.
		catch(error){
			console.log(typeof error);
			console.warn(error.message);
		}
		finally{
			// continue execution of code regardless of success and failure of code execution in the try block.
			alert('intensity updates will show new alert');
		}
	}
	showIntensityAlert(56);

	// alerat('hi');

	console.log('hi im after the showIntensityAlert');