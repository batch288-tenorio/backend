// console.log('good morning, batch 288');

// An array in programming is simply a list of data that shares the same data type.

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

// Now with an array we can simply write the code above like this

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];

// [section] arrays
	// arrays are used to store multiple related values in a single variable.
	//  they are decclared using the square barckets ([]) also know as 'array literals'

	/*
		Syntax:
			let/const arrayName = [elementA, elementB, elementC ...];

	*/

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Toshiba'];

// Possible use of an array but is not recommended

let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// alternative ways to write arrays:

let myTasks = [
		'drink html',
		'eat javascript',
		'inhale css',
		'bake sass',
	]

console.log(myTasks);

// creating an array with value from variable.

let city1 = 'Tokyo';
let city2 = 'Manila';
let city3 = 'Jakarta';

let cities = [city1, city2, city3];

console.log(cities);

city1 = 'Osaka';
console.log(cities);

// [section] length property
	//  the length property allows us to get and set the total number of items in an array.

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// .length property can also set the total number of elements in an array, meaning we can actually delete the last item in the array or shorten the array by simple updating the length of an array.

console.log(myTasks);

// myTasks.length = myTasks.length - 1;
myTasks.length -= 2;
console.log(myTasks);
console.log(myTasks.length);

// to delete a specific item in an array we can employ array methods (which will be shown/ discussed in the next session) or an algorithm.

// another example using decrementation:

console.log(cities);
cities.length--;
console.log(cities);

//  we cant do the same with strings
let fullName = 'Jamie Noble';
console.log(fullName);
fullName.length -= 1;
console.log(fullName);

	// if you can shorten the array by setting the length property, you cannot shorten the numbers of characters using the same property.

// since you can shorten the array by setting the length property, you can also lengthen it by adding a number into the length property. Since we lenthen they array forcibly, there will be another item in the array, however, it will be empty oor undefined.

let theBeatles = ['John', 'Ringo', 'Paul', 'George'];
console.log(theBeatles);
theBeatles.length += 1;
console.log(theBeatles);

// [section] reading from arrays

	/*
		-accessing array elements is one of the most common tasks that we can do with an array
		- we can do this by using the array indexes
		- each element in an array is associated with its own index/number.

			syntax:
				arrayName[index]
	*/

console.log(grades[0]);

let lakersLegend = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
console.log(lakersLegend);

// you can actually save/ store array items in another variable

let currentLaker = lakersLegend[2];
console.log(currentLaker);

// you can actually reassign array values using item indices

console.log('array before reassignment: ');
console.log(lakersLegend);

// reassign the value of specific element:

lakersLegend[1] = "Pau Gasol";
console.log('array after reassignment: ');
console.log(lakersLegend);

// accessing the last element of an array
// since the first element of an array starts at 0, subtracting 1 to the lenth of an array will offset the value by ine allowing us to get the last element.

let bullsLegend = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];

console.log(bullsLegend[bullsLegend.length-1]);

// adding elements into the array

let newArr = [];
console.log(newArr);
newArr[newArr.length] = 'Cloud strife';
console.log(newArr);

newArr[newArr.length-1] = 'Tifa Lockhart';
console.log(newArr);

newArr[newArr.length] = 'Tifa Lockhart';
console.log(newArr);

// [section] looping over an array
	// you can use a forloo to iterate over all items in an array.
console.log("_______________________________");
	// for loop
		
	for(let index = 0; index < bullsLegend.length; index++){
		console.log(bullsLegend[index]);
	}

	// example
		// Mini-activity
		// we are going to crate a loop that will check whether the lement inside our array is divisible by 5 or not.
		// if divisible by 5 console number + "is divisible by 5"
		// if not console the number + "is not divisible by 5"

	let numArr = [5, 12, 30, 46, 40];

	let numDivBy5 = [];
	let numNotDivBy5 = [];

	for(index = 0; index < numArr.length; index++ ){
		if (numArr[index] % 5 === 0 ){
			console.log(numArr[index] +' is divisible by 5');
			numDivBy5[numDivBy5.length] = numArr[index];
		}
		else {
			console.log(numArr[index] + ' is not divisible by 5');
			numNotDivBy5[numNotDivBy5.length] = numArr[index];
		}
	}

	console.log(numDivBy5);
	console.log(numNotDivBy5);

// [section] multidimensional arrays
	// multidimensional arrays are useful for storing complex data structure
	// though useful in number of cases, creating complex array structures is not always recommended.

	let chessboard = [
		['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
   	    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
        ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
        ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
        ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
        ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
		]

	console.log(chessboard);
	console.log(chessboard[0][2]);
	console.log(chessboard[3][4]);

	// two dimensional array

	console.table(chessboard);