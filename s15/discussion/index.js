// Single Line Comment

/*
	Multi Line Comment
	This is a comment

*/

// [Section] Syntax, Statements and Comments
// Statements in programming, these are the instructions that we tell computer to perform
// Javascript Statement usually it ends with semi colon (;)
// Semicolons are not required in JS, but we will use it to help us prepare for other strict languaged like java
// A syntax in programming, it is the set of rules that describes how statements must be constructed
// All lines/blocks of code should be written in a specific or else the statement will not rum

// [section] variables 
// it is used to containe / stor data
// any information that is used by an application is stored in what we call memory.
// when we create variables, certain portion of a device memory is given a name that we call variables.

// declaring variables
// declaring variiables -tells our devices that a variable name is creatd and is ready to store data

	// Syntax:
		// let/ const variableName

let myVariable;

// by default if you declare a variable and did not initialize its value it will become "undefined"

// console.log is useful for printing values of a variable or certain results of code into the google chromes browser's console
console.log (myVariable);

/*
	Guidelines in writing variables:
		1. Use the 'let' keyword followed by the variable name of your choice amd used the assignment operator(=) to assign a value.
		2. variable names should start with a lowercase character, use camelCase for multiple words.
		3. for constant variables, use the 'const' keyword
		4. variable names, it should be indicative (descriptive) of the value being stored to avoid confusion.
*/

// declaring and initializing variables
// Initializing variables - this is the instance when a variable is given its inital or starting value
	// syntax 
		 // let/ const variableName = value;

// example:
let productName = 'desktop computer';

console.log	(productName);

let	productPrice = 18999;
console.log (productPrice);

// in the context of certain application, some variables or information are constant and should not change.
//  in this example, the interest rate for a loan or savings account or mortgage must not change due to real world conerns.

const interest = 3.539;

// reassigning variable values
// reassigning a variable, it means changing its inital or previous into another value.
 	// syntax
	// variableName = newValue

productName = 'Laptop';
console.log (productName)

// the value of a variab;e declared using const kewyword cant be reassigned.

/*
interest = 4.489;
console.log (interest)*/;

// reassigning variable vs iniializing variables
// declares a variable

let supplier;
// initializing
supplier = "John Smith Tradings";
// reassigning
supplier = "Uy's Tradings";

// declaring and initializing a variable
let consumer = 'Chris';

// reassigning
consumer= 'Topher';

// can you declare a const variable withour initialization? no. an error will occur

/*const driver;

 driver = "Warlon Jay";*/

// var vs. let/const keyword
	// var is also used in declaring variable, but var is an EcmaScript 1 version (1997)
	// let/cont keyword was introduced as a new feature in ES6 (2015)

// What makes let/const different from var?
	// there are issue associated with variables declared / created using var, regarding hoisting
	// hoisting is javascript default behavior of moving declarations to the top.
	//  in terms if variables and constant, keyword var is hoisted and let and const does not allow hoisting.

	// example of hoisted:

	a = 5;
	console.log(a);
	var a;

/*	b = 6;
	console.log(b);
	let b;*/

// let/const keyword local/global scopes
	// scope essentially means where these variables are available or accessible for use

	// let and const are block scoped 
	// a block is a chunk of code bounded by {}. a block lives in a curly braces. anything within the braces are blocked.

let outerVariable = 'hello';

let globalVariable;

	{
		let innerVariable = 'hello again';
		console.log(innerVariable);
		console.log(outerVariable);

		globalVariable = innerVariable;
	}

	// console.log (innerVariable);

console.log (globalVariable);

//Multiple variable declarations and initialization.
// multiple variables maybe declated in 1 statement

let productCode = 'DC017', productBrand ='Dell';
console.log (productCode);
console.log	(productBrand);


// mutiple variables to be consoled in one line.
console.log (productCode, productBrand);

// using a variable with a reserved keyword.
// reserved keywords cannot be used as a variable name as it has function in javascript
/*const let ='Hi Im let keyword';
console.log(let);*/

// [section] data types

// strings - series of characters that create a word, a phrase a sentence or anything related to creating a text.
// string in javascript can be written using either a single quote ('') or double quote("")

let country = 'Philippines';
let province = "Metro Manila";

	// concatenating of strings in javascfript
		//multiple string values can be combined to create a single string using the "+" symbol

let fullAddres = province + ', ' +  country
console.log (fullAddres);

let greeting = 'I live in the ' + country
console.log(greeting);

/*
	-The escape characters (\) in strings in combination with other characters can produce different effects/ results.
	-"\n" this creates a next line in between text.


*/

let mailAddress ='Metro Manila\n\nPhilippines'
console.log(mailAddress);

let message = "John' s employees went home early";
console.log(message);

message = 'John\'s employee went home early.';
console.log(message)

// Numbers
// Integers/ Whole Number

let headcount = 26;
console.log(headcount);

// Decimal Number/ Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combine text and strings
console.log("John's grade last quarter is " +grade);

// Boolean
// boolean value are normally used to create values relating to the state of certain things.

let isMarried = false;
let isGoodConduct = true;
console.log("isMarried: " +isMarried);
console.log("isGoodConduct: " +isGoodConduct);

// arrays
// arrays are special kind of data that's usded to stor multiple related values.
// In Javascript, arrays can store different data types but is normally used to store similar data types.

// similar data types 
// syntax:
	// let/cosnt arrayName = [elementA, elementB, elementC, ...];

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data types 
let details = ["John", "Smith", 32, true];
// not recommended in using array
console.log(details);

// objects
// objects are another special kind of data  type that is used to mimic real world objects/items.
// 

/*
	Syntax: 
	let/const objectName = {
		propertyA: valueA,
		propertyB: valueB,
	}
*/

let person = {
	fullName: "Juan Dela Cruz", 
	age: 35,
	isMarried: false,
	contact: ["+63917 123 4567", "8123 4567"],
	address: {
		houseNumber:'345',
		city:'Manila'
	}
}

console.log(person);

// type of operator, is used to determine the type of data or value of a variable. It ourputs string

console.log(typeof mailAddress);
console.log(typeof headcount);
console.log(typeof isMarried);

console.log(typeof grades);

// Note: Array is a special type of object with methods and function to manipulate

// constant objects and arrays
/*
The keyword const is a little misleading

It does not define a constant value. It defines a constant reference to a value:

Because of this you can not:
reassign a constant value.
reassign a constatnt array
reassign a constant object

but you can:

change the elements of a constant array
change the properties of a constant object
*/

const anime = ["One piece", "One Punch Man", "Attach on Titan"];

/*anime = ["One piece", "One Punch Man", "Kimetsu no yaiba"];
console.log(anime);*/

anime[2] = "Kimetsu no yaiba";
console.log(anime);

// Null 
// It is used to intentionally express the absence of a value in a variable.

// re assign

let spouse = null;
spouse = "Maria";

// Undefined
// represents the state of a variable that has been declared but without an assigned value.


// Initialize
let fullName;
fullName = "Maria";