// console.log('hello World!');

// Functions
	// Functions in JavaScript are line/blocks of codes that tell our device/ application to perform a certain task when called/ invoke.
	// Functions are mostly created to create complicated tasks to run several lines of code in succession.

// Function declarations
	// defines a function with the specified parameters
	// syntax:
		/*
			function  functionName() {
				codeBlock/statements;
			}
		*/

	// function keyword - used to defined a Javascript function
	// functionName - the function name. fnction are named tobe able to use later in the code for the invocation 
	// function block ({}) - the statements which comprise the body of the function. This is where the code will be exected.

		// example:
			function printName(){
				console.log("My Name is John!");
			}

// Function Invocation
	// the code block and statements inside a function is not immediately excuted when the function is defined. the cobeblock and statements inside the function is executed when the function is invoked or called.
	// It is common to use the term "call a function" instead of "invoke a function"

			// let's invoke/call the function that we declare.
			printName();

// function declaration vs expresion
	// function declaration
		// a function can be created thorugh function declaration by busing the using the funciton keyword and adding the function name.
		// declared function/s can be hoisted

			declaredFunction();
			// Note: Hostin is a javascript behavior for certain variables and functions to run or use before thier declaration


			function declaredFunction(){
				console.log('Hello World from declaredFunction!');
			}

	// function expression
			// a function can alsob be stored in a variable.
			//  a function expression is an anonymous fucntion assigned to a variable function.
			

			let variableFunction = function(){
				console.log('Hello Again!');
			};

			variableFunction();

			// We can also create a function expression of a named function.

			let funcExpression = function funcName(){
				console.log('hello from the other side!');
			};

			funcExpression();

			// you can reassign declared function and function expression to new anonymous function.

			declaredFunction = function(){
				console.log('updated declaredFunction')
			};

			declaredFunction();

			funcExpression = function (){
				console.log('updated funcExpression');
			};
			funcExpression();

			// however, we cannot reassign a function expression initialized with const.

			const constantFunc = function(){
				console.log('initialized with const!');
			};
			constantFunc();

			/*	constantFunc = function(){
				console.log('cannot be reassigned!');
			};
			constantFunc();*/

// Function Scoping
	// Scope it is the accessibility of variables within our program.

	/*
		JavaScript Variables has 3 types of scope:
		1. Global
		2. Local/ block scope
		3. Function Scope
	*/

			{

				let localVar = "Armando Peres";
			}

			// Result in error. locaVar, being in a block, it will not be accessible outside the block.
			/*console.log(localVar);*/

			let globalVar = "Mr. Worldwide";

			{
				console.log(globalVar);
			}

		// function scope
			// javascript has function scope: each function creates a new scope
			// variables defined inside a function are not accessible (visible) from outside the function.

			function showNames(){
				const functionConst = "John";
				let functionLet = "Jane";

				console.log(functionConst);
				console.log(functionLet);
			}

			/*console.log(functionConst);*/
			/*console.log(functionLet);*/

			showNames();

		// Nested functions
			// you can created another function inside a function. this is called a nested function

			function myNewFunction(){
				let name = "Jane";

				// nested function
				function nestedFunction(){
					console.log(name);
				}
				nestedFunction();
			}
			myNewFunction();

			let functionExpression = function(){
				function nestedFunction(){
					let greetings = "Hello Batch 288!";
					console.log(greetings);
				}
				nestedFunction();
			}

			functionExpression();

// Return Statement
	/*
		The return statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.
	*/

			function returnFullName(){
				let fullName = "Jeffrey Smith Bezos";
				let returnArray = [1, 2, 3, 4, 5]
				return(returnArray);
				

			}
			returnFullName();
			console.log(returnFullName());

			let fullName = returnFullName();
			console.log(fullName);

// function naming conventions
	// function names should be definitive of the task it will perform. it usually contains a verb.

		function getCourses(){
			let courses = ['science 101', 'match 101', 'english 101'];
			return course;
		}

		// avoid generic names to avoid confusion within our code.

		function get(){
			let name = 'jamie';
			return name;
		}

		// avoid pointless and inappropriate fnction names

		function foo(){
			return 25%5;
			
		}

		// name your functions in a small caps. Follow camelCase when naming variables and functions with multiple words.

		function displayCarInfo(){
			console.log('Brand: Toyota');
			console.log('Type: Sedan');
		}

	// returning nested function

		function trialFunction(){
			function nestedFunction(){
				let name = 'Lebron';

				return name;
			}
			console.log(nestedFunction());
			return nestedFunction();
		}

		console.log(trialFunction());