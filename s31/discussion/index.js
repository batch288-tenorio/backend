// Use the 'require' directive to load Node.js modules 
// Module is a software component or part of a program that contains one or more routines.
// The "http module" lets node.js transfer data using the Hypertext transfer protocol
// http is a protocol that allows the fetching of resources such as HTML documents

// clients (browser) and servers (node js / express js application) communicate by exchanging individual messages.
//  these messages are sent by the clients, usually a web browser and called "request"

// the messages sent by the server as an answer are called "response"
let http = require("http");

// using this module's createServer() method, we can create an HTTP server that listens to request on a specified port and gives responses back to the client.
//  a port is a virtual point where network connections starts and end.
//  the http module has a createServer() method that accepts a function as an argument and allows for creation of a server
//  the arguments passed in the function are request and response onjects (data types) that containes methods that allows us to receive request from the client and send responses back to it.

// the server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to it eventually communicating with our server.
http.createServer(function (request, response) {

	// use the writeHead() method to:
	// set a status code for the response - a 200 means OK
	// set the content-type of the response as a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'});

	//send the response with text content 'Hellow World?!'
	response.end('Hellow World?!');

}).listen(8000);

// when the server is running, console will print the message:
console.log('Server running at localhost:8000');