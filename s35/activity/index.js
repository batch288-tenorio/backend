const express = require('express');
// Mongooes is a package that allows creation of schemas to model our data structures
// also has access to a number of methods for manipulating our database
const mongoose = require('mongoose');

const app = express();

const port = 3001;

	// SECTION: MongoDB Connection
	// Connect to the database by passing your connection string 
	// Due to update in Mongo DB drivers that allow connection, the default connection string is beging flagges as an error
	// by default a warning will be displayed in the terminal when the application is running.
	// {useNewUrlParser:true}

	// Syntax:
	// mongoose.connect("MongoDB string", {useNewUrlParser:true});

	mongoose.connect("mongodb+srv://admin:admin@batch288tenorio.oxntzoo.mongodb.net/batch288-todo?retryWrites=true&w=majority", {
		useNewUrlParser: true
	});

	// Notification whether the we connected properly with the database.

	let db = mongoose.connection;

	// for catching the error just in case we had an error during the connecton
	// console.error.bind allows us to print errors in the browser and in the terminal
	db.on("error", console.error.bind(console, "Error! Can't connect to the Database"));

	// if the connection is successful
	db.once("open", ()=> console.log("We're connected to the cloud database!"));

	// Middlewares
	// allows our app to read json data
	app.use(express.json());
	// allows our app to read data from forms
	app.use(express.urlencoded({extended:true}))


	// Section - Mongoose Schemas
	// Schemas determine the structure of the document to be writeen in the database
	// schemas act as blueprint to our data
	// use the Schema() constructor of the mongoose module to create a new schema object

	const taskSchema = new mongoose.Schema({
		// Define the fields with the corresponding data type
		name: String,
		// lets add another field which is status
		status: {
			type: String,
			default: "pending"
		}
	})

	// Section- Models
	// uses schema and are used to create/instantiate objects thats corresponds to the schema
	// models use schema and they act as the middleman from the server(JS code) to our database.

	// to create a model we are going to use the model();
	// Task - capital letter to know that this is model
	const Task = mongoose.model('Task', taskSchema);

	// Section - Routes

	// Create s POST route to create a new task
	// Create a new task 
	// Business Logic:
		// 1. Add a functionality to check whether there are duplicate task
			// if the task is existing in the database, we return an error
			// if the task doesnt exist in the database, we add it in the database
		// 2. The task data will be coming from the request's body

	app.post("/tasks", (request, response) => {
		// "findOne" method is a mongoose method that acts similar to "find" in MongoDB.
		// if the findOne finds a document that matches the criteria it will return the object/document and if there's no object that matches the criteria it will retrn an empty object or null.
		console.log(request.body.name);
		Task.findOne({name: request.body.name})
		.then(result =>{
			// we can use if statement to check or verify whether we have an object found.
			if(result !== null){
				return response.send("Duplicate task found!");
			}
			else{

				// create a new task and save it to the database.
				let newTask = new Task({
					name: request.body.name
				})

				// the save() method will store the information to the database
				// since the newTask was created from the Mongoose Schema and Task Model, it till be save in tasks collection
				newTask.save()

				return response.send('New task created!')
			}

		})
	})

	// Get all the tasks in our collection
	// 1. retrieve all documents 
	// 2. If an error is encountered, print the error
	// 3. If no error/s is/are found, send a success status to the client and show the documents retrieved

	app.get("/tasks", (request, response) => {
		// find method is a mongoose method that is similar to MongoDb find
		Task.find({})
		.then(result => {
			return response.send(result);
		})
		.catch(error => response.send(error));
	})

	const userSchema = new mongoose.Schema({
		username: String,
		password: String
	})

	const User = mongoose.model('User', userSchema);

	app.post("/signup", (request, response) =>{
		console.log(request.body.username);
		User.findOne({username: request.body.username})
		.then(result =>{
			if(result !== null){
				return response.send("Duplicate username found");
			}
			else{
				if(request.body.username != "" && request.body.password != ""){
					let newUser = new User({
						username: request.body.username,
						password: request.body.password
					})

					newUser.save()
					.then(save => {
						return response.status(201).send('New User Registered')		
					})
					.catch(error => response.send(error))
					
				}
				else{
					response.send('Both username and password must be provided')
				}
			}
		})	
	})

if(require.main === module){
	app.listen(port, () => {
		console.log(`Server is running at port ${port}!`)
	})
}

module.exports = app;
