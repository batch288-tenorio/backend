// console.log("Hello World!");
	
	function printInput(){
		let nickname = "Chris";

		console.log("Hi, " + nickname);
	}

	printInput();
	printInput();

	// For other cases, functions can also process data directly passed into it insted of relyingonly on Global Variables.

	// consider this function:

	function printName(name){
		console.log("My name is " + name);
	}

	printName("Juana");
	printName();

	// variables can also be passed as an argument

	let sampleVariable = "Curry";

	let sampleArray = ["Davis ", "Green ", "Jokic ", "Tatum "];

	printName(sampleVariable);
	printName(sampleArray);

	// One example of using the parameter and argument 
		// functionName(number);

	function checkDivisibilityBy8(num){
		let remainder = num % 8

		console.log("the remainder of " + num + "divided by 8 is: " + remainder);

		let isDivisibleBy8 = remainder === 0 ;

		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);

	// Functions as argument
		// function parameters can also accept other functions as arguments.

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed!");
	}

	function invokeFunction(func){
		func();
	}

	invokeFunction(argumentFunction);

	// function as argument with return keyword

	function returnFunction(){
		let name = "Chris";
		return name;
	}

	function invokedFunction(func){
		console.log(func());
	}

	invokedFunction(returnFunction);

	// using multiple parameters
	// multiple "arguments" will correspond to the number of "parameters" declared in a fuction in succeding order

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + " " + middleName + " " + lastName);
	}
	createFullName('Juan', 'Dela', 'Cruz');

	// In javascript, providing more/ less arguments than the expected will  not return an error

	// providing less arguments than the expected parameters will automatically assign an undefined value to the parameter
	createFullName('Gemar', 'Cruz');

	createFullName('Lito', 'Masbate', 'Galan', 'Jr.');

	// In other programming languages this will return an error satating that "the number of arguments do not match the number of parameters".

	// Using variable as multiple argments

	let firstName = 'John';
	let middleName = 'Doe';
	let lastName = 'Smith';

	createFullName(firstName, middleName, lastName);

/*
alert - notif
prompt - with user input (string)
*/

// Using Alert()
	// alert allows us to show a small window at the top of our browser page to show information to our users. As opposed to console.log which only shows the message on the console

	// alert("hello World!"); //this will run immediately when the page loads.
	// syntax: alert('messageInString')

	function showSampleAlert(){
		alert("Hello, user!");
	}

	//showSampleAlert();
	console.log("I will only log in the console when the alert is dismissed");

// Using prompt()
	// allows us to show a small window at the top of the browser to gather user input.
	// the value gathered from a prompt is returned as a string
	// syntax: prompt("dialogInString")

	/*let samplePrompt = prompt("Enter your name.");
	console.log(typeof samplePrompt);
	console.log("Hello, " + samplePrompt);*/

	/*let age = prompt("Enter your age.");
	console.log(age);
	console.log(typeof age);*/

	// returns an empty string when there is no input or null if the user cancels the prompt.
	/*let sampleNullPrompt = prompt('Dont enter anything');
	console.log(sampleNullPrompt);*/

	function printWelcomeMessage(){
		let firstName = prompt('Enter you first Name.');
		let lastName = prompt('Enter your last name');

		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log('Welcome to my page!');
	}
	printWelcomeMessage();