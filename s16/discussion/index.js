// To check whether the script is properly linked to your html file.
/*console.log('Hello Batch 288!');*/

// Section - Arithmetic Operators
 let x = 1397;
 let y = 7831;

 // Addition Operator (+)
 let sum = x + y;
 console.log("Result of addition Operator: " + sum);

 // Subtraction Operator (-)
 let difference = y - x;
 console.log("Result of subtraction Operator: " + difference);

 //  Multiplication Operator (*)
 let product = x * y;
 console.log("Result of multiplication operator: " + product);

 // Division Operator (/)
 let quotient = y / x;
 console.log("Result of division operator: " + quotient);

 // Modulo Operator (%)
 let remainder = y % x;
 console.log("Result of modulo operator: " + remainder);

 // [Section] Assignment Operators
 	// Assignment Operator (=)
 	// The assignment operator assigns/reassigns the value to the varaiable.	

 let assignmentNumber = 8;

 // the addition assignment operator (+=)
 //  the addition assignment operator adds the vlaue of the right operand to a variablr and assigns the result to the variable.

 assignmentNumber += 2;
 console.log(assignmentNumber);

 assignmentNumber += 3;
 console.log("Result of addition assignment operator: " + assignmentNumber);

 //  Subtraction/ Multiplication / Division Assignment Operator (-=, *=, /=)

 // subtraction assignment operator
 assignmentNumber -= 2;
 console.log("Result of subtraction assignment operator: " + assignmentNumber);

 // multiplication assignment operator
 assignmentNumber *= 3;
 console.log("Result of multiplication assignment operator: " + assignmentNumber);

// Division Assignment Operator
 assignmentNumber /= 11;
console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators and parentheses
	// MDAS - mutiplication or division first then addition or subtraction, from left to right

	let mdas = 1 + 2 - 3 * 4 / 5;

	/*
		1. 3 * 4 = 12
		2. 12 / 5 = 2.4
		1 + 2 - 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6
	*/

	console.log(mdas.toFixed(1));

	let pemdas = 1 + (2-3) * (4/5);

	/*
		1. 4/5 = 0.8
		2. 2-3 = -1
			1 + (-1) * 0.8
		3. -1 * 0.8 = -0.8
		4. 1 - 0.8
	*/

	console.log(pemdas.toFixed(1));

// [Section] Increment and Decrement
		// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment and decrement applied to.

		let z = 1;
		let increment = ++z;

		console.log("Result of pre-increment: " + increment);

		console.log("Result of pre-increment: " + z);

		increment = z++;
		console.log("The result of post-increment: " + increment);

		console.log("The result of post-increment: " + z);

		x = 1;
		let decrement = --x;

		console.log("Result of pre-decrement: " 
			+ decrement);
		console.log("Result of pre-decrement: " 
			+ x);

		decrement = x--;
		console.log("Result of post-decrement: " + decrement);
		console.log("Result of post-decrement: " + x);

// [Section] Type Coercion

		/*
		-Type Coercion is the automatic or implicit conversion of values from one data type to another.
		- This happens when operations are performed on different types that would normally possible yield irregular results.
		*/

		let numA = '10';
		let numB = 12;

		let coercion = numA + numB;
		// if your are going to add string and number, it will concatenate its value
		console.log(coercion);
		console.log(typeof coercion);

		let numC = 16;
		let numD = 14;

		let nonCoersion = numC + numD;
		console.log(nonCoersion);
		console.log(typeof nonCoersion);

		// The boolean "true" is also associated with the value of 1.
		let numE = true + 1;
		console.log(numE);

		// The boolean "false" is also associated with the value of 0.
		let numF = false + 1;
		console.log(numF);

// [SECTION] Comparison Operators

		let juan = 'juan'

		// Equality Operator (==)
		/*
			- Checks wether the operand are equal / have the same content/value.
			- Attempts to CONVERT AND COMPARE operands of different data types.
			- Returns a Boolean value.
		*/

		console.log(1 == 1); //true
		console.log(1 == 2); //false
		console.log(1 == '1'); //true 
		console.log(0 == false); //true
		// Compare two strings that are the same
		console.log('JUAN' == 'juan'); //false (case-sensitive)
		console.log(juan == 'juan');

		// Inequality Operator (!=)
		/*
			-Check wether the operands are not equal / have differenet content/ values.
			- Attempts to CONVERT AND COMPARE operands of different data types.
			-Returns boolean value.
		*/
		console.log(1 != 1); //false
		console.log(1 != 2); //true
		console.log(1 != '1'); //false
		console.log(0 != false); //false
		console.log('JUAN' != 'juan'); //true
		console.log(juan  != 'juan'); //false

		// Strict Equality Operator (===)
		/*
			-Check wether the operands are equal / have the same content.
			-also COMPARE the data types of two values.
		*/	
		console.log(1 === 1); //true
		console.log(1 === 2); //false
		console.log(1 === '1'); //false
		console.log(0 === false); //false
		console.log('JUAN' === 'juan'); //false
		console.log(juan === 'juan'); //true

		// Strictly Inequality Operator (!==)
		/*
			- Check wether the operands are not equal/have different content.
			- also COMPARES the data types of 2 values.
		*/

		console.log(1 !== 1); //false
		console.log(1 !== 2); //true
		console.log(1 !== '1'); //true
		console.log(0 !== false); //true
		console.log('JUAN' !== 'juan'); //true
		console.log(juan !== 'juan'); //false

// [SECTION] Relational Operators
// -Checks wether one value is greater than or less than to the other value.

		let a = 50;
		let b = 65;

		// GT or Greater than operator (>)
		let isGreaterThan = a > b; //false
		// LT or Less Than Operator (<)
		let isLessThan = a < b; //true
		// GTE or greater than or equal operator (>=)
		let isGTorEqual = a >= b; //false
		// LTE or less thank or equal (<=)
		let isLTorEqual = a <= b; //true

		console.log(isGreaterThan);
		console.log(isLessThan);
		console.log(isGTorEqual);
		console.log(isLTorEqual);

		let numStr = '30';
		console.log(a > numStr);
		console.log(b <= numStr);

		let strNum = "twenty"; //20
		console.log( b >= strNum); //false
		// since the string is not numeric, the string was converted to a number and it resulted to NaN (Not a Number);

// [SECTION] Logical Operators

		let isLegalAge = true;
		let isRegistered = false;

		// Logical AND operator (&& - Double Ampersand)
		// Return "true" if all operands are true.
		// isRegistered = true;
		let allRequirementsMet = isLegalAge && isRegistered;//false

		console.log("Result of logical AND operatos: " +allRequirementsMet);

		// Logical OR operator (|| - Double Pipe)
		// Returns true if one of the operands are true
		// isLegalAge = false;
		let someRequirementsMet = isLegalAge || isRegistered;
		console.log('Result of logical OR operator: '+ someRequirementsMet);

		// Logical NOT Operator (! - Exclamation Point)
		// Returns the opposite value.
		let someRequirementsNotMet = !isRegistered;
		console.log('Result of logicial NOT Operator: ' + someRequirementsNotMet);


