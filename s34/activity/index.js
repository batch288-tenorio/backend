// Use the require directive to load the express module /package
// It will allow us to access methods and fuctions that will help us create easily our application or server.
// a 'module' is a software component or part of a program that contains one or more routines.
const express = require("express");

// create an application using express
// creates an express application and stores this is a constant variable called app.
const app = express();

// for ou application server to run, we need a port to listen
const port = 4000;

// Middlewares
	// is software that provides common services and capabilities to application outside of what's offered by the operating system.
	// allow our application to read json data
	app.use(express.json());

	// This one will allow us to read data from forms.
	// by default, information receive from the URL can also be received as a string or an array
	// by applying the option of 'extended : true' this will allow us to receive information in other data types such as an object which will use throughout our application
	app.use(express.urlencoded({extended: true}));

	// Section: Routes
	// Express has methods corresonding to each http method
	// this route expects to receive a GET request at the base URI "/"
	app.get("/", (request, response) => {
		// Once the route is accessed it will send a string response containing "Hello World!"

		// compared to the previous session, .end uses the node JS module method
		// .send method uses the express JS module's method instead to send a response back to the client.

		response.send('Hello Batch 288!');

	})

	// this route expects to receive a GET request at the URI "/hello"
	app.get("/hello", (request, response) => {
		response.send('Hello from the /hello endpoint!');
	})

	// other samples

	app.get("/hello2", function(request, response) {
		response.send('Hello from the /hello endpoint!');
	})

	function helloFunction(request, response){
		response.send('Hello from the /hello endpoint!');
	}
	app.get("/hello3", (request, response) => {
		helloFunction(request, response);
	})

	// this route expects to receive a POST request

	app.post("/hello", (request, response) => {
		// request.body contains the contents/data of the request body
		// all the properties defined in our POSTMAN request  will be accessible here as properties with the same name.
		console.log(request.body);

		response.send(`hello there ${request.body.firstName} ${request.body.lastName}`);
	})

	// an array will store user objects when the "/signup" route is accessed
	//  this will serve as our mock database
	let users = [];

	app.post("/signup", (request, response) => {
		console.log(request.body);

		if(request.body.username !== "" && request.body.password !== ""){
			users.push(request.body);
			response.send(`User ${request.body.username} successfully registered!`)
		}
		else{
			response.send('Please input BOTH username and password');
		}

		// response.send("Trial");
	})

	// this route expects to receive a PUT request at the URI "/change-password"
	// this will update the password of a user that matches the information provided in the client/postman
	app.put("/change-password", (request, response) => {

		let message;

			for(let index = 0; index < users.length; index++ ){

				// if the provided username in the client/postman and the username of the current object in the loop is the same
				if(request.body.username == users[index].username){
					
					users[index].password = request.body.password

					message = `User ${request.body.username}'s password has been updated!`

					break;
				}
				else{
					message = 'User does not exist!'
				}
			}
			response.send(message);

	})

	// Activity

	app.get("/home", (request, response) => {
		response.send('Welcome to the homepage');
	})

	app.get("/users", (request, response) => {
		response.send(users);
	})

	app.delete("/delete-user", (request, response) => {
		let message;

		if (users.length === 1){
			for(let index = 0; index < users.length; index++ ){
				if(request.body.username === users[index].username){
					users.splice(index, 1);
					message = `User ${request.body.username} has been deleted`;
					break;
				}
				if(message === undefined){
					message ='User does not exist';
				}
				else{
					message = 'No user found';
				}
			
			}
		}
		response.send(message);
	})


	// tell our server to listen to the port
	// if the port is accessed, we can run the serve
	//  return a message confirming that the server is running in teh terminal
	

	if(require.main === module){
		app.listen(port, () => console.log(`Server running at ${port}`));
	}
	module.exports = app;