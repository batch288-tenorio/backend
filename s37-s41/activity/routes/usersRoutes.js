// para magamit router 
const express = require('express');
const usersControllers = require('../controllers/usersControllers.js');

const auth = require("../auth.js");

const router = express.Router();

//routes
router.post("/register", usersControllers.registerUser);

// login
router.post("/login", usersControllers.loginUser);

router.get("/userDetails", auth.verify, usersControllers.retrieveUserDetails);

router.get("/details", auth.verify, usersControllers.getProfile);

// route for course enrollment
router.post('/enroll', auth.verify, usersControllers.enrollCourse);

// token verification
// router.get("/verify", auth.verify);

module.exports = router;