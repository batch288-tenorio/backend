const express = require('express');

const coursesControllers = require('../controllers/coursesControllers.js');
const auth = require('../auth.js')

const router = express.Router();

// This route is responsible for adding course in our db.

// Without Parameters
router.post('/addCourse', auth.verify, coursesControllers.addCourse);

// route for retrieving all courses
router.get('/', auth.verify, coursesControllers.getAllCourses);

// route for retrieving all courses
router.get('/activeCourses', coursesControllers.getActiveCourses);

// Route for inactive courses
router.get('/inActiveCourses', auth.verify, coursesControllers.getInactiveCourses);


// With Parameters
// route for getting the specific course information
router.get('/:courseId', coursesControllers.getCourse);

// route for updating course
router.patch('/:courseId', auth.verify, coursesControllers.updateCourse);

router.patch('/:courseId/archive', auth.verify, coursesControllers.archiveCourse);
;



module.exports = router;