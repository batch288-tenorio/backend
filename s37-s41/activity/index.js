const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = 4001;
const usersRoutes = require('./routes/usersRoutes.js');
const coursesRoutes = require('./routes/coursesRoutes.js');
// it will allow our backend application to be available to our front end application
// it will also allows us to control the app's cross origin resource sharing settings.
const cors = require('cors');

// mongoDb connection
// establish the connection between the database and the application
// the name of the database should be :"CourseBookingAPI"
mongoose.connect("mongodb+srv://admin:admin@batch288tenorio.oxntzoo.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

const db = mongoose.connection;

	db.on("error", console.error.bind(console, "Error, can't connect to the db!"));

	db.once("open", () => console.log("We are now connected to the db!"));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
// reminder that we are going to use this for the sake of the bootcamp
app.use(cors());

// add the routing of the routes from the usersRoutes
app.use('/users', usersRoutes);
app.use('/courses', coursesRoutes);


if(require.main === module){
	app.listen(process.env.PORT || 4000, () => {
	    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
	});
}

module.exports = {app,mongoose};
